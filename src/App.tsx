import { useState } from "react"
import { useEffect } from "react"
import { useCallback } from "react"
import reactLogo from "./assets/react.svg"
import viteLogo from "/vite.svg"

import words from "./assets/wordList.json"

import { HangmanDrawing } from "./components/HangmanDrawing.tsx"
import { HangmanWord } from "./components/HangmanWord.tsx"
import { InfoPanel } from "./components/InfoPanel.tsx"
import { Keyboard } from "./components/Keyboard.tsx"
import { RestartGameButton } from "./components/RestartGameButton.tsx"

function getWordToGuess() {
  return words[Math.floor(Math.random() * words.length)]
}

function App() {
  const [wordToGuess, setWordToGuess] = useState(getWordToGuess())

  const [guessedLetters, setGuessedLetters] = useState<string[]>([])

  const incorrectLetters = guessedLetters.filter(
    letter => !wordToGuess.includes(letter)
  )

  const maxGuesses = 6
  const isLoser = incorrectLetters.length >= maxGuesses
  const isWinner = wordToGuess.split("").every(letter => guessedLetters.includes(letter))
  const numGuessesRemaining = maxGuesses - incorrectLetters.length
  const isGameOver = isLoser || isWinner

  const successAudio = new Audio("../assets/success.wav")
  const failAudio = new Audio("../assets/oof.wav")
  const winnerAudio = new Audio("../assets/win.wav")
  const loserAudio = new Audio("../assets/lose.wav")

  const addGuessedLetter = useCallback((letter: string) => {
    if (guessedLetters.includes(letter) || isWinner || isLoser) return

    if (wordToGuess.includes(letter)) {
      successAudio.play()
    } else {
      failAudio.play()
    }

    setGuessedLetters(currentLetters => [...currentLetters, letter])
  }, [guessedLetters])

  const resetGame = useCallback(() => {
    setGuessedLetters([])
    setWordToGuess(getWordToGuess())
  }, [])

  useEffect(() => {
    if (isWinner) {
      winnerAudio.play()
    } else if(isLoser) {
      loserAudio.play()
    }
  }, [isWinner,isLoser])

  useEffect(() => {
    const handler = (e: KeyboardEvent) => {
      const key = e.key

      e.preventDefault()

      if (key.match(/^[a-z]$/)) {
        addGuessedLetter(key)
      } else if (isGameOver && key == "Enter") {
        resetGame()
      }
    }

    document.addEventListener("keypress", handler)

    return () => {
      document.removeEventListener("keypress", handler)
    }
  }, [guessedLetters])

  return (
    <div
      style={{
        maxWidth: "800px",
        display: "flex",
        flexDirection: "column",
        gap: "2rem",
        margin: "0 auto",
        alignItems: "center",
      }}
    >
      <InfoPanel
        isWinner={isWinner}
        isLoser={isLoser}
        numGuessesRemaining={numGuessesRemaining}
      />
      <HangmanDrawing numBadGuesses={incorrectLetters.length} />
      <HangmanWord
        revealWord={isLoser}
        guessedLetters={guessedLetters}
        wordToGuess={wordToGuess}
      />
      <div style={{ alignSelf: "stretch" }}>
        <Keyboard
          isGameOver={isGameOver}
          activeLetters={guessedLetters.filter(letter => wordToGuess.includes(letter))}
          inactiveLetters={incorrectLetters}
          addGuessedLetter={addGuessedLetter}
        />
      </div>
      <RestartGameButton isGameOver={isGameOver} isWinner={isWinner} resetGame={resetGame} />
    </div>
  )
}

export default App
