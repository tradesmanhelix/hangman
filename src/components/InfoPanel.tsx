import styles from "../styles/InfoPanel.module.css"

type InfoPanelProps = {
  numGuessesRemaining: number
  isWinner: boolean
  isLoser: boolean
}

export function InfoPanel({ numGuessesRemaining, isWinner, isLoser }: InfoPanelProps) {
  if (isWinner) {
    return (
      <div
        className={`
          ${styles.infoPanel}
          ${styles.gameWonPanel}
        `}
      >
        🎉 Winner! Refresh to try again
      </div>
    )
  } else if (isLoser) {
    return (
      <div
        className={`
          ${styles.infoPanel}
          ${styles.gameLostPanel}
        `}
      >
        😭 😭 😭<br/>Click the button, press enter, or<br />refresh to try again
      </div>
    )
  } else {
    return (
      <div
        className={`
          ${styles.infoPanel}
          ${numGuessesRemaining > 4 ? styles.okGuesses : ""}
          ${numGuessesRemaining <= 4 ? styles.warningGuesses : ""}
          ${numGuessesRemaining <= 2 ? styles.dangerGuesses : ""}
        `}
      >
        {numGuessesRemaining} { numGuessesRemaining > 1 ? "guesses" : "guess"} left!
      </div>
    )
  }
}
